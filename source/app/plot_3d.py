from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

import numpy as np

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

X =  np.load('../date/tsne_3.npy')
y = np.load('../date/y_train.npy')
ax.scatter(X[:,0],X[:,1], X[:,2],c = y )

ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')

labelTups = [('Setosa', 0), ('Versicolour', 1), ('Virginica', 2)]
for name, label in labelTups:
    ax.text3D(X[y == label, 0].mean(),
              X[y == label, 1].mean() + 1.5,
              X[y == label, 2].mean(), name,
              horizontalalignment='center',
              bbox=dict(alpha=.5, edgecolor='w', facecolor='w'))

ax.legend( 
          loc='center left', bbox_to_anchor=(1.0, .5))

plt.show()