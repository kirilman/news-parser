# import sqlite3
import html_parser as parser
import datetime

DB_PATH = "/home/kirilman/Projects/news-parser/dbase.db"
print('Тест')
class generator():
    def __init__(self):
        self.referens_for_visit = []
        # self.dbase = sqlite3.connect(DB_PATH)
        self.generator_list = []
        # sql = 'select href from generator' 
        # db_cursor = self.dbase.cursor()
        # db_cursor.execute(sql)
        # res = db_cursor.fetchall()

    def get_referens_for_visit(self,dbase):
        db_cursor = dbase.cursor()
        sql = 'select href from generator' 
        db_cursor.execute(sql)
        res = db_cursor.fetchall()
        self.generator_list = [x[0] for x in res]

    def process_link(self, url, dbase):

        db_cursor = dbase.cursor()
        #id generator
    
        db_cursor.execute("SELECT generator_id FROM generator WHERE href = %s",[url])
        res = db_cursor.fetchall()[0]
        generator_id = res[0]

        list_refs = parser.get_reflist_by_url(url)
        internal_refs = parser.drop_external_refs(url,list_refs)
        count_new_link = 0
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        for ref in internal_refs:
            try:
                db_cursor.execute("""select count(href) from reference_for_visit where href = %s """, [ref])
                sql_res = db_cursor.fetchall()[0]
                # print('Количество сылок ', sql_res)
            except Exception as err:
                print("Неудалось получить сылки из БД: ", err, '\n', ref)
                continue
            # else:
            #     dbase.commit()   

            if sql_res[0] != 0:
                # print("update ref_visit ",ref)
                db_cursor.execute("UPDATE reference_for_visit SET life_count = 0 WHERE href = %s", [ref])
                dbase.commit()
            else:
                db_cursor.execute("INSERT INTO reference_for_visit(generator_id, href, date_add, life_count) \
                    VALUES (%s, %s, %s, %s)",[generator_id,ref,current_time, 0])
                dbase.commit()
                count_new_link+=1

        db_cursor.execute("SELECT href FROM reference_for_visit where generator_id = %s",[generator_id])
        refs_from_base = [x[0] for x in db_cursor.fetchall()]
        difference = set(refs_from_base) - set(internal_refs)
        diff = [tuple([x]) for x in difference]

        try:
            db_cursor.executemany("UPDATE reference_for_visit SET life_count = life_count + 1 WHERE href = %s ",diff)
            dbase.commit()
        except Exception as err:
            print("Неудалось обновить life_count для новых сылок", err, '\n')
            return []
        
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        # print('Перед history')
        try:
            db_cursor.execute("INSERT INTO generator_history(generator_id, date_add, count_new_href) \
                          VALUES (%s, %s, %s)",[generator_id, current_time, count_new_link])
            print('Добавлено {} ссылок с {}'.format(count_new_link, generator_id))
            dbase.commit()
        except Exception as err:
            print(generator_id," ", current_time)
            print('Неудалось обновить историю генератора ', err)
            dbase.commit()

        # self.dbase.close()
        return internal_refs