import manager as man
import time
from configurator.config import read_configuration
from base.data_base import dbase_adapter

if __name__ == "__main__":
    #Cчитать параметры
    settings = read_configuration()
    print(settings)
    #База
    dbase = dbase_adapter(settings)
    dbase.initialize_base(settings['tables'])
    
    i=0
    # exit()
    while True:
        man.run(2,dbase.connection);
        i += 1
        print('\n Итерация {} \n'.format(i))
        time.sleep(10)