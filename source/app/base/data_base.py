import sqlite3
import psycopg2


class dbase_adapter():
    def __init__(self, settings):
        self.connection = self.create_base_connect(settings)
        self.cursor = self.connection.cursor()

    def create_base_connect(self,settings):
        connect = psycopg2.connect(dbname=settings['base_name'],
                        user = settings['user'],
                        password = settings['password'], 
                        host = settings['host']
                        )
        return connect

    def execute(self,sql):
        self.cursor.execute(sql)


    def initialize_base(self, table_names):
    
        create_request = {'generator': "CREATE SEQUENCE auto_generator_id; \
                                    CREATE TABLE \"generator\" ( \
                                    \"generator_id\"	INTEGER NOT NULL PRIMARY KEY \
                                     DEFAULT nextval('auto_generator_id'), \
                                    \"href\"	TEXT NOT NULL)",

                        'generator_history': "CREATE TABLE \"generator_history\" ( \
                                \"generator_id\"	INTEGER NOT NULL, \
                                \"date_add\"	TEXT NOT NULL, \
                                \"count_new_href\"	INTEGER NOT NULL, \
                                PRIMARY KEY(generator_id,date_add) )",
                        'reference_for_visit': "CREATE TABLE \"reference_for_visit\" ( \
                                \"generator_id\"	INTEGER NOT NULL, \
                                \"href\" TEXT NOT NULL, \
                                \"date_add\"	TEXT NOT NULL, \
                                \"life_count\"	INTEGER, \
                                PRIMARY KEY(href))"
        }

        self.cursor.execute("SELECT table_name FROM information_schema.tables WHERE table_schema='public'")

        res = self.cursor.fetchall()
        tables = [x[0] for x in res]

        difference = list(set(table_names) - set(tables))
   

        if len(difference) == 0:
            return
        else:
            for table in difference:
                self.cursor.execute(create_request[table])
                self.connection.commit()
        return 