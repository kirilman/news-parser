
import configparser
import ast


def read_configuration():
    config_file = 'config.conf'

    config = configparser.RawConfigParser()
    if (len(config.read(config_file)) == 0):
        print('Файл конфигурации отстуствует')
        config.add_section("base")
        config.add_section("monitor")

        # config.set("base","data_base_path","/home/kirilman/Загрузки/base.db")
      
        config.set("base", "base_name", "news")
        config.set("base", "user", "postgres")
        config.set("base", "password","606613")
        config.set("base", "host","localhost")

        config.set("base","tables", "generator, reference_for_visit, generator_history")
        config.set("monitor","period","4")

        config.write(open(config_file,"w"))
        print('Файл конфигурации создан - {}'.format(config_file))

    else:
        print('{} - файл конфигурации прочитан'.format(config_file))


    settings = dict()
    for i in config.sections():
        for option in config[i].items():
            if option[0] == 'tables':
                settings.update({option[0]: ast.literal_eval(option[1]) } )
            else:
                settings.update({option[0]:option[1]})

    return settings