import pandas as pd
import re
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
# import matplotlib.patches as mpatches

from sklearn.decomposition import PCA, TruncatedSVD
import matplotlib

import seaborn as sns

ria = pd.read_csv('../data.csv')

ria.drop_duplicates(inplace=True)
ria.head()
print(ria.shape)

eco_task = pd.read_csv('../ecology_task.csv')
eco_task.dropna(inplace=True)
eco_task.head()
print(eco_task.shape)

frame = pd.concat([ eco_task, ria],ignore_index=True)
frame.head()
print(frame.shape)

frame.head()

def cv(data):
    count_vectorizer = CountVectorizer()

    embedding = count_vectorizer.fit_transform(data)

    return embedding, count_vectorizer

list_corpus = frame["text"].tolist()
list_labels = frame["label"].tolist()

X_train, X_test, y_train, y_test = train_test_split(list_corpus, list_labels, test_size=0.2, 
                                                                                random_state=40,)

X_train_counts, count_vectorizer = cv(X_train)
X_test_counts = count_vectorizer.transform(X_test)

def plot_LSA(test_data, test_labels, plot=True):
        lsa = TruncatedSVD(n_components=2)
        lsa.fit(test_data)
        lsa_scores = lsa.transform(test_data)
#         temp = pd.DataFrame(lsa_scores)
        flatui = ["black", "#3498db", "red", "#e74c3c", "#34495e", "#2ecc71"]
    
        sns.scatterplot(x=lsa_scores[:,0],y=lsa_scores[:,1], legend='full', 
                        cmap="Greens", hue=np.array(test_labels), 
                        style = test_labels, sizes = test_labels,
                       )
#         sns.scatterplot(temp)
#         plt.scatter(lsa_scores[:,0], lsa_scores[:,1], c=test_labels, s = 12 , alpha=0.7,cmap="Spectral")
        
#         color_mapper = {label:idx for idx,label in enumerate(set(test_labels))}
#         color_column = [color_mapper[label] for label in test_labels]
#         colors = ['orange','green']
#         if plot:
#             plt.scatter(lsa_scores[:,0], lsa_scores[:,1], s=8, alpha=.8, c=test_labels,cmap=color_column)
#             red_patch = mpatches.Patch(color='orange', label='Ria')
#             green_patch = mpatches.Patch(color='green', label='Ecology_task')
#             plt.legend(handles=[red_patch, green_patch], prop={'size': 14})
        plt.legend()
        return lsa_scores
# sns.set_style("whitegrid")
# sns.set(style="ticks")


X_embedded = np.load('../tsne_2.npy')
y_train = np.load('../y_train.npy')
fig = plt.figure(figsize=(15, 15), dpi = 120)          

sns.scatterplot(x=X_embedded[:,0],y=X_embedded[:,1], legend='full', 
                        cmap="Greens", hue=np.array(y_train), 
                        style = y_train, sizes = y_train,
                       )

# r = plot_LSA(X_train_counts, y_train)
plt.show()