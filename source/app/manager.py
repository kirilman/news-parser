import html_parser as parser
import datetime
import time 
import generator as gen

DB_PATH = "/home/kirilman/Projects/news-parser/dbase.db"

def run(dT, dbase):
    print('Run: dT = ',dT)
    db_cursor = dbase.cursor()
    db_cursor.execute('select * from generator')
    list_generts = [x for x in db_cursor.fetchall()]
    if len(list_generts) == 0:
        print("Нету сайтов генераторов для обхода")
        return
    main_gener = gen.generator()
    map_generts = dict()

    for generator_id, ref in list_generts:
        db_cursor.execute("SELECT generator_id, date_add FROM generator_history  WHERE generator_id = %s \
                        ORDER BY date_add DESC LIMIT 1",[generator_id])
        res = db_cursor.fetchall()
        
        if len(res) == 0:
            print('Блок 2: Обработать ',ref)
            main_gener.process_link(ref, dbase)
            return
        else:
            map_generts.update({ref:res[0][1]})

    new_map_generts = dict()
    current_time = datetime.datetime.now()
    for generator, date in map_generts.items():
        d = current_time - datetime.datetime.strptime(date,"%Y-%m-%d %H:%M:%S")
        print(d.seconds)
        if d.seconds > 60*dT: #10 минут
            new_map_generts.update({generator:date})
            print('Больше заданого времени +')
        else:
            print('Меньше заданого времени')

    print('Before 4')
    if len(new_map_generts) != 0:
        old_ref = sorted(new_map_generts.items(), key=lambda x:x[1])[0][0]
        print('Блок 4: Обработать ',old_ref)

        main_gener.process_link(old_ref,dbase)
        # dbase.close()
        # return

    print(map_generts)

    old_ref, most_old_date = sorted(map_generts.items(), key=lambda x:x[1])[0]

    most_old_date =  datetime.datetime.strptime(most_old_date,
                                                "%Y-%m-%d %H:%M:%S") 

    # print('s',(most_old_date + datetime.timedelta(minutes=dT))) 
    t_delta = datetime.datetime.now().timestamp() - (most_old_date + datetime.timedelta(minutes=dT)).timestamp() 
    print('Самая старая дата:', most_old_date, 'делта :', t_delta)
    if t_delta > 0:
        print('>')
        main_gener.process_link(old_ref,dbase)

    else: #время не прошло спать
        print('<')
        print('Спать ', abs(t_delta))
        time.sleep(abs(t_delta))
        main_gener.process_link(old_ref,dbase)
        # dbase.close()
        return
        