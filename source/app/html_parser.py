import re
import requests
import urllib
from bs4 import BeautifulSoup, SoupStrainer


def get_html(url):
    page = requests.get(url,verify=False)
    return page.text


# def get_reflist_by_url(url):
#     try:
#         page_text = get_html(url)
#     except Exception as err: 
#         print('Неудалось получить html по {}'.format(url))
#         print(err)
#         return []
#     lines = page_text.split('\n')
#     list_refs = []
#     for line in lines:
#         res_1 = re.findall("http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", line)
#         res_2 = re.findall('s*(?i)hrefs*=s*(\"([^\"]*\")|\'[^\']*\'|([^\'\">s]+))',line)
#         res_2 = [x[0] for x in res_2]
#         result = list(set(res_1).union(res_2))
#         for ref in result:
#             if len(ref) == 0:
#                 continue
#             else:

#                 if ref[0] == "'" and ref[-1] == "\'":
#                     ref = ref[1:-1]
#                     list_refs.append(ref)
#                     continue
#                 if ref[0] == "\"" and ref[-1] == "\"":
#                     ref = ref[1:-1]
#                     list_refs.append(ref)
#                     continue
#                 if ref[0] == "\"" or ref[0] == "'":
#                     ref = ref[1:]
#                 if ref[-1] == "\"" or ref[0] == "'":
#                     ref = ref[:-1]
#                 list_refs.append(ref)
#     # print('Всего найдено href :',len(list_refs))
#     return list_refs

def get_reflist_by_url(url):
    try:
        page_text = get_html(url)
    except Exception as err: 
        print('Неудалось получить html по {}'.format(url))
        print(err)
        return []
    soup = BeautifulSoup(page_text, 'html.parser', parse_only=SoupStrainer('a'))

    list_refs = [link['href'] for link in soup if link.has_attr('href')]
    # print('Всего найдено href :',len(list_refs))
    return list_refs

def is_right_ref(ref):
    nedfull_extens = ['html','xhtml','htm','php','shtml']
    for i,ch in enumerate(ref[::-1]):
#         print(s,i)
        if ch == '/':
            pos = len(ref) - i - 1
            break
    if pos==len(ref)-1:
        return True

    sub_str = ''
    has_dot = False
    for i,ch in enumerate(ref[pos:]):
        if ch == '.':
            has_dot = True
            for k,ch in enumerate(ref[pos+i+1:]):
                sub_str+=ch
                if sub_str in nedfull_extens:
                    return True
                if k > 5:
                    break

    if has_dot:
        return False
    else:
        return True
            
    

def clear_reflist(list_refs):
    new_refs = []
    list_reqex = ['.css$','.png$','.ico$','.jpeg$']
    # exess_link = set()

    for ref in list_refs:
        # if re.search('^#',ref) == None and re.search("png$",ref) == None:
        # if re.search():
        temp = [ref for reqex in list_reqex if re.search(reqex, ref) == None ]
        new_refs+=temp
    # print(len(new_refs))
    return list(set(new_refs))

def drop_external_refs(main_url,list_refs):
    if len(main_url) == 0 or main_url[-1] == '/':
        print('Не правильный домен ', main_url); return []
    if len(list_refs) == 0:
        print('Список входных сылок пуст'); return []
    main_domain = urllib.parse.urlsplit(main_url).netloc
    new_refs = []
    for _, ref in enumerate(list_refs):
        if (urllib.parse.urlsplit(ref).netloc == main_domain):
            new_refs.append(ref)
        else:
            if len(ref) == 0:
                continue
            if re.search("^\/[^\/]", ref ) != None:
                new_refs.append(main_url + ref)
            if ref[0] == '.':
                new_refs.append(main_url + ref[1:])

    clear_reflist = list(set([x for x in new_refs if  is_right_ref(x)]))
    return clear_reflist


def get_text_by_url(url):
    try:
        page = get_html(url)
    except Exception: 
        print('Неудалось получить html по {}'.format(url))
        return None
    lines = page.split('\n')
    lines_with_text = []
    flag=True
    for line_no_format in lines:
        line=""
        for ch in line_no_format:
            if ch=='<':
                flag=False
                sub_lines=re.findall('[А-яёЁ, .0-9-;:"]{100,}',line)
                for sub_line in sub_lines:
                    lines_with_text.append(sub_line)
                   # lines_with_text.append(line)
                line=""
                continue
            if ch=='>':
                flag=True
                continue
            if flag == True:
                line+=ch
    text = ''
    for x in lines_with_text:
        text+=x
    if text!=None and len(text)!=0:
        return text
    else:
        None